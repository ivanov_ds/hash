#include <iostream>
#include <memory> // для умных указателей  make_unique
#include <string.h> // тут стринги

// подключаем библиотеку для шифрования.
#include "./sha3.h"

// стандартное значение для 16-ных файлов, установлено самим создателем билиотки.
#define FROMHEX_MAXLEN 256

// если нашли, то флаг будет тру.
volatile bool isFound = false;
// сам ответ
long long answer;


// переводим из строки в 16-ричный формат. меньше места и быстрее сравнивается для машины.
const uint8_t *fromhex(const char *str)
{
	static uint8_t buf[FROMHEX_MAXLEN];
	size_t len = strlen(str) / 2;
	if (len > FROMHEX_MAXLEN) len = FROMHEX_MAXLEN;
	for (size_t i = 0; i < len; i++) {
		uint8_t c = 0;
		if (str[i * 2] >= '0' && str[i*2] <= '9') c += (str[i * 2] - '0') << 4;
		if ((str[i * 2] & ~0x20) >= 'A' && (str[i*2] & ~0x20) <= 'F') c += (10 + (str[i * 2] & ~0x20) - 'A') << 4;
		if (str[i * 2 + 1] >= '0' && str[i * 2 + 1] <= '9') c += (str[i * 2 + 1] - '0');
		if ((str[i * 2 + 1] & ~0x20) >= 'A' && (str[i * 2 + 1] & ~0x20) <= 'F') c += (10 + (str[i * 2 + 1] & ~0x20) - 'A');
		buf[i] = c;
	}
	return buf;
}

// проверка модуля, чтоб не подключать cmath
template<class T>
T abs(T a) {
	return a > 0 ? a : -a;
}

// высчитывает расстояние, а т.е. разницу между двумя хешами (кот создали мы, и который дал нам препод.)
int distance(const uint8_t *a, const uint8_t *b, uint8_t len) {
	int d = 0;
	for (uint8_t i = 0; i < len; ++i) {
		// сумма разностей всех по модулю.
		d += abs<int>((int)(a[i] - b[i])) ;
	}
	return d;
}

// функция проверки.на вход принимает строку чаров как пароль, его удлину, и то, с чем мы должны сравнить (то что дал нам препод)
bool check(uint8_t *pass, size_t len, const char *hash) {
	uint8_t digest[SHA3_256_DIGEST_LENGTH];

	sha3_256(pass, len, digest); // само шифрование, все в библиотеке.
	if (distance(fromhex(hash), digest, (int)32) == 0) { // смотрим на совпадение, только при 0
		return true;
	}
	return false;
}


// функция выполняется в параллеле. на вход принимает пароль в виде числа, и хеш который ищем.
bool threadFunc(long long  pass, std::string target) {
	auto pass_s = std::to_string(pass); // пароль превратили в строку, синтаксис с++11, будет std::string

	// проверяем на вход даем массив char-ов ( c_str() делает из std::string массив чаров)
	// далее длина нашего пароля
	// то, с чем должны сравнить, тоже берем массив чаров.
	if (check( (uint8_t *) pass_s.c_str(), pass_s.length(), (const char *) target.c_str() )) {
		// если нашли, то записываем пароль, указываем что нашли, и возвращаем значение тру.
		answer = pass;
		std::cout << "OK\n";
		std::cout << pass << "\n";
		return true;
	} 
	return false;
}

// класс ну ясн.
class Threads {
public: 
	// конструктор кот принимает то, что должны найти, и максимальную длину пароля.
	Threads(std::string target, long long max_pass) : m_target(target), m_max_pass(max_pass) {	};

	void run() {
		// говорим что этот участок исполнятеся параллельно в 3 потока и общая переменная isFound
		#pragma omp parallel for num_threads(3) shared(isFound)
		// перебираем все возможные пароли
		for (long long current_pass = 0; current_pass < m_max_pass; ++current_pass) {
			// если еще не нашли то проверяем 
			if (!isFound)
				if (threadFunc(current_pass, m_target)) {
					// ставим флаг что нашли, и указываем какой пароль.
					isFound = true;
					current_pass = m_max_pass;
				}			
		}
	}
private:
	std::string m_target; // что должны найти.
	long long m_max_pass; // макс длина пароля.
};

int main(void) {
	// начиная с с++11 auto сам определяет тип.

	// максимальная длина пароля.
	auto max_len = 1000000000;
	// тот хеш который ищем.
	auto hash = "499a13771e49e1b7574a915db1f56ea00c599a27f73c57f8a0fccd28b5c22529"; // 356
	// обернем все в исключение, мало ли что может выскочить, но в параллеле их отловить не возможно!
	try {
		// создаем экземпляр класса указывая что ищем и макс длину пароля.
		auto task = std::make_unique<Threads>(hash, max_len); // это с++14
		// запускаем поиск.
		task->run();

		// если нашли пишем об этом, если нет, то тож
		if (isFound) std::cout << "the password is " << answer << std::endl;
		else std::cout << "no found the password" << std::endl;
	} catch (std::exception e) {
		// если было брошено исключение, то напишем какое.
		std::cout << e.what();
	}
	return 0;
}
